# MustangX



MustangX is a Content Management System
[MustangX (CMS)](http://mustangx.org/)
and licensed under the [MIT License](LICENSE).

This distribution is contains all the main ingredients needed for most common
use cases, and can be extended with plugins(widgets or modules)and themes.

 The MustangX project is planeed to be built from scratch or at least with *NO FRAMEWORK*.


## Requirements

* Commining soon



## Documentation

For the install guide and reference, see:

* Non yet. Will be done as Dev steps are completed.
* See also the Development wiki.

* A nice READ and what this is based on <a href="https://github.com/PatrickLouys/no-framework-tutorial" target="_blank">IS FOUND HERE</a>


## Contributing

Pull requests are welcome. Please see our [CONTRIBUTING](CONTRIBUTING.md) guide.



Thanks to
[everyone who has contributed](CONTRIBUTORS.md) already.
